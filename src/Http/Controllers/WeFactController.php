<?php

namespace HollanderICT\WeFact;

Class WeFactClass
{
    /**
     * Set the API Key
     *
     * @var $api_key
     */
    private $api_key;

    /**
     * Set the API Endpoint
     *
     * @var
     */
    private $api_endpoint;

    public function __construct()
    {
        $this->api_key = env('WEFACT_API_KEY', '');
        $this->api_endpoint = env('WEFACT_API_KEY', 'https://api.mijnwefact.nl/v2/');
    }

    private function sendRequest($controller, $action, $params)
    {
        if (is_array($params)) {
            $params['api_key'] = $this->api_key;
            $params['controller'] = $controller;
            $params['action'] = $action;
        }

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->api_endpoint);

        // Check certificate?
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, '10');
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($params));
        $curlResp = curl_exec($ch);
        $curlError = curl_error($ch);

        if ($curlError != '') {
            $result = array(
                'controller' => 'invalid',
                'action' => 'invalid',
                'status' => 'error',
                'date' => date('c'),
                'errors' => array($curlError)
            );
        } elseif (curl_getinfo($ch, CURLINFO_HTTP_CODE) == 403) {
            $result = array(
                'controller' => 'invalid',
                'action' => 'invalid',
                'status' => 'error',
                'date' => date('c'),
                'errors' => array($curlResp)
            );
        } else {
            $result = json_decode($curlResp, true);
        }

        return $result;
    }

    public function createDebtor()
    {
        return $this->sendRequest('debtor', 'add', [
            'CompanyName' => 'Company X',
            'CompanyNumber' => '123456789',
            'TaxNumber' => 'NL123456789B01',
            'Sex' => 'm',
            'Initials' => 'John',
            'SurName' => 'Jackson',
            'Address' => 'Keizersgracht 100',
            'ZipCode' => '1015 AA',
            'City' => 'Amsterdam',
            'Country' => 'NL',
            'EmailAddress' => 'info@company.com',
            'PhoneNumber' => '010 - 22 33 44',
            'AccountNumber' => 'NL59RABO0123123123',
            'AccountBIC' => 'RABONL2U',
            'AccountName' => 'Company X',
            'AccountBank' => 'Rabobank',
            'AccountCity' => 'Amsterdam'
        ]);
    }
}